# Knative examples

Examples of using Knative and deploying functions from GitLab

## Nodejs Echo function

This is a Echo function written in Node and using a Node.js knative runtime.

Once running, test the echo function with something like the _Advanced REST client_, see the snapshot below:

![](./img/arcgitlab.png)

or via curl with a command like the one below:

```
curl -H 'Content-type: application/json' -d '{"gitlab":"rocks"}' http://knative-test-echo.<YOUR_DOMAIN_DOT_COM>
{"gitlab":"rocks"}
```

## Go container application

This is a simple Go application packaged as a container and build using a Dockerfile.
Knative uses the kaniko build template to build the container corresponding to this application.

Once built you can open your browser at the URL of the knative service and you will see a _Hello_ header.

Or via _curl_:

```
curl http://knative-test-container.<YOUR_DOMAIN_DOT_COM>
<h1>Hello</h1>
```

